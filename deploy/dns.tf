data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}." # Data refrence within Terraform
}

resource "aws_route53_record" "app" {
  # Add subdomains to zone
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}" # e.x: api.dev.thomas5566gb.com
  type    = "CNAME"                                                                            # Canonical name links it to another existing DNS name
  ttl     = "300"                                                                              # Time to live

  records = [aws_lb.api.dns_name]
}

# In order to use HTTPS and encrypt our traffic, we need to create an HTTPS certificate
# acm: Amazon certificate manger
# fqdn: Fully qualified domain name
resource "aws_acm_certificate" "cert" {
  domain_name       = aws_route53_record.app.fqdn
  validation_method = "DNS"

  tags = local.common_tags

  lifecycle {
    create_before_destroy = true # Keep Terraform running smooth
  }
}

resource "aws_route53_record" "cert_validation" {
  # Domain validation
  # You assign a value to a DNS record within that domain
  # domain_validation_options = an attribute returned by the AWS certificate
  # 0 = take the first potion by specifying 0 index
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]
  ttl = "60" # shortest amount of time that you can make a certificate live
}

# aws_acm_certificate_validation used to trigger the validation process in AWS 
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn # Certificate we want to validate
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
