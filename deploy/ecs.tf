# Assiging the permissions in order to start tasks
resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

# Assigning the permissions in order to start tasks
# assign the task execution role which gives ecs
# ecs permissions that it needs in order to start a new task
# task-exec-role.json => to retrieve the image from the Docker repository
# and create & put logs in to the log streams
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-exec-role-policy"         # Create new IAM policy in our AWS account
  path        = "/"                                             # Route path
  description = "Allow retrieving of images and adding to logs" # What our task exec role does
  policy      = file("./templates/ecs/task-exec-role.json")
}

# Assigning the permissions in order to start tasks
resource "aws_iam_role" "task_execution_role" {
  name               = "${local.prefix}-task-exec-role"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# Assigning the permissions in order to start tasks
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn # Sets the policy
}

# Giving permissions to our task that it needs at run time
resource "aws_iam_role" "app_iam_role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}

data "template_file" "api_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {
    app_image                = var.ecr_image_api
    proxy_image              = var.ecr_image_proxy # URI for our images in ECR
    django_secret_key        = var.django_secret_key
    db_host                  = aws_db_instance.main.address # Hostname of database
    db_name                  = aws_db_instance.main.name
    db_user                  = aws_db_instance.main.username
    db_pass                  = aws_db_instance.main.password
    log_group_name           = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region         = data.aws_region.current.name
    allowed_hosts            = aws_route53_record.app.fqdn # Allowed host to our load balancer address, Defines the list of allowed hosts names
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

resource "aws_ecs_task_definition" "api" {
  family = "${local.prefix}-api" # name of the task definition
  # rendered: passed in all of the values from data "templates_file" "api_container_definitions"
  container_definitions    = data.template_file.api_container_definitions.rendered
  requires_compatibilities = ["FARGATE"] # Make our ECS task compatible with FARGATE
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  # Gives the permissions to execute a new task
  execution_role_arn = aws_iam_role.task_execution_role.arn
  # Role given to actual running task
  task_role_arn = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}

resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS Service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  # Allow outbound access from our container on port 443
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound access from our service to our database
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  # Access inbound connections from public internet to prxy
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"
    # cidr_blocks = ["0.0.0.0/0"]
    security_groups = [
      aws_security_group.lb.id
    ]
  }

  tags = local.common_tags
}

resource "aws_ecs_service" "api" {
  name            = "${local.prefix}-api"
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.api.family
  desired_count   = 1         # the number of tasks wish to run inside the service
  launch_type     = "FARGATE" # Allows you to run tasks without managing the servers

  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    # Assigns security group to give it appropriate natwork access
    security_groups = [aws_security_group.ecs_service.id]
    # assign_public_ip = true # Assign a public IP address
  }

  # Tells our ECS service to register new tasks with our target group
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  # set dependencies between resources to ensure that terraform 
  # create of resource in the right order
  depends_on = [aws_lb_listener.api_https]
}

data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl") # Creates new data resource

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

resource "aws_iam_policy" "ecs_s3_access" {
  # Create a new policy
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the recipe app S3 bucket"

  policy = data.template_file.ecs_s3_write_policy.rendered
}

resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}
