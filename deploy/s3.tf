# Creating a new Terraform resource
resource "aws_s3_bucket" "app_public_files" {
  bucket_prefix = "${local.prefix}-files"
  acl           = "public-read" # Access control list
  force_destroy = true          # Easily destroys bucket with Terraform
}