resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    # Allows you to add multiple subnets to you database
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  # in order to see the name in the AWS console
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

resource "aws_security_group" "rds" {
  # Security groups allow you th control the inbound and outbound access allowed to that resource
  description = "Allow access to the RDS database instance."
  # Security groups to manage the RDS inbound access
  name   = "${local.prefix}-rds-inbound-access"
  vpc_id = aws_vpc.main.id

  # egress - outbound access
  # ingress - inbound access
  ingress {
    # controls what the rules are for the inbound access
    protocol  = "tcp"
    from_port = 5432 # default port for Postgres
    to_port   = 5432

    # Limit down further to restricted to just the access form Bastion instance
    # At this stage don't need to allow in ingress sccess from anywhere in our private network
    # Limit to connections from our bastion server
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id, # Give access to our ECS service from our database
    ]
  }

  tags = local.common_tags
}

# Create an RDS instance in AWS
resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db" # Database identifier used to access the database in our console
  name                    = "recipe"             # Used to define the database name that is create within our instance
  allocated_storage       = 20                   # Assigns how much disk space is given to the database
  storage_type            = "gp2"                # Entry-level storage type in AWS
  engine                  = "postgres"           # Specifies the type of engine
  engine_version          = "11.4"               # Specifies the type of engine
  instance_class          = "db.t2.micro"        # Type of database server we want to run
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0                           # Set the number of the day you want to maintain backups for database
  multi_az                = false                       # Determines if the database should be run on multiple avalilability zones
  skip_final_snapshot     = true                        # if remove the database then it will create a final snapshot of the database
  vpc_security_group_ids  = [aws_security_group.rds.id] # set permissions on accessing database through the network

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
