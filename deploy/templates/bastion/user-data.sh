#!/bin/bash

sudo yum update -y                         # Update yum package manager with latest packages
sudo amazon-linux-extras install -y docker # install docker
sudo systemctl enable docker.service       # Enable docker
sudo systemctl start docker.service        # start Docker on bastion system
sudo usermod -aG docker ec2-user           # Add our EC2 user to the Docker group