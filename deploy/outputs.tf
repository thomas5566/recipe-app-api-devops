# Output of a certain variable or attribute on one of our resources
output "db_host" {
  # internal network address for our database
  value = aws_db_instance.main.address
}

# Output Bation public DNS name
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

output "api_endpoint" {
  # Create a new output value for our load balancer dns name onec
  # it's  been created by Terraform 
  # value = aws_lb.api.dns_name
  value = aws_route53_record.app.fqdn # Create the output of our API endpoint
}