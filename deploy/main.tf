terraform {
  backend "s3" {
    bucket         = "recipe-app-devops-thomas5566"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

# local are ways that you can create dynamic variables inside Terraform
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    # Tags applied to every resource we create in Terraform
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    MangedBy    = "Terraform"
  }
}

# retrieve the current region that we are applying terraform to
data "aws_region" "current" {}
