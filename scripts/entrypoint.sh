#!/bin/sh

# -e: if there's any error of the commands exit the script immediately
set -e

# Collect static: Allows you to collect all of the static files ewquired
# for ypu project and store them in a single directory
# --noinput: suppress any questions being asked to the user
# when run the command and it will go ahead and assume yes to everything
python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# uwsgi: Name of the application
# --socket :9000: Run uWSGI as a TCP socket on port 9000
# This way can map the request from our proxy to this port using uWSGI port
# --workers 4: specify the number worker we want to run for uWSGI service
# --master: Run this as the master service on the terminal
# --enable-threads: enable multi-threads
# --module: Actual application that uWSGI is going to run
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi